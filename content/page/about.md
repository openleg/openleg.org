---
title: About the OpenLeg Project
subtitle: Turning sour lemons into sweet lemonade!
comments: false
---

The leg is no good no more and it's gotta go. I would still like to be able to walk though, and an alternative better than simple prosthetics needs to happen. My wonderful friends will help me build an amazing leg and - as a benefit to all - we will make the hardware and software available here.

Happy leg hacking!
